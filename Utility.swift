//
//  Utility.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/8/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps


class Utility
{
    static var typeUser:TypeUser = TypeUser.Guest
    static var insidentType:InsidentType = InsidentType.None
    
    static var visualEffect: UIVisualEffectView!
    static var loadingCircle: UIActivityIndicatorView!
    static var currentView:UIView! = nil
    
    static var localCoordinate:CLLocationCoordinate2D!
    static var myLocality:String!=""
    
    ///New Incident Information
    static var incidentCoordinate:CLLocationCoordinate2D! 
    static var incidentThoroughfare:String!
    static var incidentPlace:String!
    static var incidentLocality:String!
    static var incidentCountry:String!
    static var incidentAdministrativeArea:String!
    
    ///Facebook Info
    static var userFacebookID:String!
    static var userFacebookName:String!
    
    //static var canExecuteAgain! = false
    
    static func alertMessage(vc: UIViewController, title: String, message: String, completion: ((UIAlertAction) -> Void)? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: completion))
        
        vc.presentViewController(alertController, animated: true, completion: nil)
    }
    
    static func showLoadingScreen(view:UIView!)
    {
        if(currentView != nil)
        {
            hideLoadingScreen()
        }
        
        currentView = view
        
        visualEffect = UIVisualEffectView(frame: currentView.frame)
        loadingCircle = UIActivityIndicatorView(frame: currentView.frame)
        
        visualEffect.addSubview(loadingCircle)
        
        currentView.addSubview(visualEffect)
        
        visualEffect.backgroundColor=UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.6)
        
        loadingCircle.startAnimating()
        
    }
    
    static func resetIncidentValues()
    {
        self.incidentCountry = nil
        self.incidentLocality = nil
        self.incidentPlace = nil
        self.incidentThoroughfare = nil
        self.incidentAdministrativeArea = nil
    }
    
    static func hideLoadingScreen()
    {
        if currentView == nil { return }
        
        visualEffect.hidden = true
        loadingCircle.stopAnimating()
        
        for subview in currentView.subviews{
            
            if(subview == visualEffect)
            {
                subview.removeFromSuperview()
            }
        }
        
        visualEffect = nil
        loadingCircle = nil
        currentView = nil
        
    }
    
    static func getPlaceName() ->String
    {
        if (incidentPlace != nil && incidentPlace != "nil")
        {
            return incidentPlace!
        }
        if (incidentThoroughfare != nil && incidentThoroughfare != "nil")
        {
            return incidentThoroughfare!
        }
        
        return "Unknow location"
    
    }
    
    static func getReferenceeName() ->String
    {
        if (incidentThoroughfare != nil  && incidentThoroughfare != getPlaceName())
        {
            return incidentThoroughfare!
        }
        if (incidentLocality != nil && incidentLocality != "nil")
        {
            return incidentLocality!
        }
        if (incidentAdministrativeArea != nil && incidentAdministrativeArea != "nil")
        {
            return incidentAdministrativeArea!
        }
        if (incidentCountry != nil && incidentCountry != "nil")
        {
            return incidentCountry!
        }
        
        return "Unknow Reference"
        
    }
    
    
    
}

enum TypeUser
{
    case Oculer,Guest
}

enum InsidentType
{
    case None,Vial,Security,Emergency,Municipal
}
