//
//  SelectNewMapLocationController.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/24/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class SelectNewMapLocationController: UIViewController,CLLocationManagerDelegate, GMSMapViewDelegate
{
    weak var firstViewController : ReportNewIncidentViewController?
    let locationManager:CLLocationManager =  CLLocationManager()    
    var mapView:GMSMapView?
    var placePicker: GMSPlacePicker?
    
    
    var placesClient: GMSPlacesClient?
   
    
    var currentPosition:CLLocationCoordinate2D?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient()
       
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        self.mapView = GMSMapView(frame: self.view.bounds)
        //mapView!.mapType = kGMSTypeHybrid
        mapView!.delegate = self
        
        //self.view.insertSubview(mapView!, atIndex: 0)
        //self.view = mapView
        
              

        // Do any additional setup after loading the view.
    }
    
    var isLoadPickerMap:Bool = false
    func loopLoadMap()
    {
        if(!isLoadPickerMap)
        {
            do
            {
                try loadChooseLocationScreen()
                NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "loopLoadMap", userInfo: nil, repeats: false)
                isLoadPickerMap=true
            }
            catch
            {
                
            }
        
        }
    }
    
    func onChangeOrientation()
    {       
        
        mapView!.frame = self.view.bounds
    }
    
//    @IBAction func getCurrentPlace(sender: UIButton)
//    {
//        Utility.showLoadingScreen(self.view)
//       
//        
//        placesClient?.currentPlaceWithCallback({
//            (placeLikelihoodList: GMSPlaceLikelihoodList?, error: NSError?) -> Void in
//            if let error = error {
//                print("Pick Place error: \(error.localizedDescription)")
//                return
//            }
//            
//            self.nameLabel.text = "No current place"
//            self.addressLabel.text = ""
//            
//            if let placeLicklihoodList = placeLikelihoodList {
//                let place = placeLicklihoodList.likelihoods.first?.place
//                if let place = place
//                {
//                    if place.formattedAddress != nil
//                    {
////                        print("getCurrentPlace")
////                        print(place.coordinate)
////                        print(place.name)
////                        print(place.formattedAddress)
//                       
//                        self.nameLabel.text = place.name
//                        self.addressLabel.text = place.formattedAddress.componentsSeparatedByString(", ")[0]
//                    }
//                    else
//                    {
//                        self.nameLabel.text = place.name
//                        self.addressLabel.text = "there is no street name here"
//                    }
//                    
//                }
//            }
//            
//            self.firstViewController!.placeNameRef.text = self.nameLabel.text
//            self.firstViewController!.streetNameRef.text = self.addressLabel.text            
//            
//            Utility.hideLoadingScreen(self.view)
//        })
//        
//        
//    }
    
    func loadChooseLocationScreen()
    {
        let center = locationManager.location!.coordinate
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        
        placePicker = GMSPlacePicker(config: config)
        
        placePicker?.pickPlaceWithCallback({ (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                
                if place.formattedAddress != nil
                {
                    //                    print("pickPlace")
                    //                    print(place.coordinate)
                    //                    print(place.name)
                    //                    print(place.formattedAddress)
                    
                    self.firstViewController!.placeNameRef.text = place.name
                    self.firstViewController!.streetNameRef.text = place.formattedAddress.componentsSeparatedByString(", ")[0]

                    
                }
                else
                {
                    self.firstViewController!.placeNameRef.text = place.name
                    self.firstViewController!.streetNameRef.text = "there is no street name there"
                }
                
            }
            else
            {
                self.firstViewController!.placeNameRef.text = "No place selected"
                self.firstViewController!.streetNameRef.text = "..."
                
            }
            //self.dismissViewControllerAnimated(true, completion: nil)
        })
        
        
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D)
    {
        currentPosition = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        
    }
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            self.mapView!.myLocationEnabled = true
            self.mapView!.settings.myLocationButton = true
            
        }
        
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            
            self.mapView!.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
            
            
        }
        
    }

    
    
    override func viewWillAppear(animated: Bool)
    {
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "onChangeOrientation", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "loopLoadMap", userInfo: nil, repeats: true)
        
        //loadChooseLocationScreen()
        
    }
    
    override func viewDidDisappear(animated: Bool)
    {
         NSNotificationCenter.defaultCenter().removeObserver(self, name: "onChangeOrientation", object: nil)
    }
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
