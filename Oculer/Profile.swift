//
//  Profile.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
import CoreData

/**
 Enum for Profile Entity member fields
 */
enum ProfileAttributes : String {
    case
    shareFacebook       = "shareFacebook",
    showMunicipal       = "showMunicipal",
    showEmergency       = "showEmergency",
    showVial            = "showVial",
    showSecurity        = "showSecurity",
    distancePost        = "distancePost"
    static let getAll = [
        shareFacebook,
        showMunicipal,
        showEmergency,
        showVial,
        showSecurity,
        distancePost
    ]
}

@objc(Profile)

/**
The Core Data Model: Profile
*/
class Profile: NSManagedObject {
    @NSManaged var shareFacebook: Bool
    @NSManaged var showMunicipal: Bool
    @NSManaged var showEmergency: Bool
    @NSManaged var showVial: Bool
    @NSManaged var showSecurity: Bool
    @NSManaged var distancePost: Int16
}