//
//  PercistenceManager.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
import CoreData


class PersistenceManager: NSObject {
    
    private var appDelegate:AppDelegate
    private var mainContextInstance:NSManagedObjectContext
    
    //Patron Singleton solo se inicializa una vez.
    class var sharedInstance: PersistenceManager {
        struct Singleton {
            static let instance = PersistenceManager()
        }
        
        return Singleton.instance
    }
    
    override init(){
        appDelegate = AppDelegate().sharedInstance()
        mainContextInstance = ContextManager.init().mainManagedObjectContextInstance
        super.init()
    }
    
   
    func getMainContextInstance() -> NSManagedObjectContext {
        return self.mainContextInstance
    }
    
    func saveWorkerContext(workerContext: NSManagedObjectContext){
        //Persist new Profile to datastore (via Managed Object Context Layer).
        do {
            try workerContext.save()
        } catch let saveError as NSError {
            print("save minion worker error: \(saveError.localizedDescription)")
        }
    }
    
  
    func mergeWithMainContext(){
        do {
            try self.mainContextInstance.save()
        } catch let saveError as NSError {
            print("synWithMainContext error: \(saveError.localizedDescription)")
        }
    }
    
}