//
//  mainController.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/8/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import AddressBookUI


class mainController: UIViewController,CLLocationManagerDelegate, GMSMapViewDelegate {
    
    let locationManager:CLLocationManager =  CLLocationManager()
    
    var mapView:GMSMapView?
    var placesClient: GMSPlacesClient?
    
    var allMarkers:[GMSMarker]! = []
    var allIncidents:[Incident]! = []
    
    var isUpdateMarkers:Bool! = false
    var canMakeFirstRequest:Bool! = false
    var endFirstRequest:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placesClient = GMSPlacesClient()
        
        locationManager.delegate = self
        //locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        self.mapView = GMSMapView(frame: self.view.bounds)
        //mapView!.mapType = kGMSTypeHybrid
        mapView!.delegate = self
        
        
        self.view.insertSubview(mapView!, atIndex: 0)
        
        if(Utility.typeUser == TypeUser.Guest)
        {
            Utility.alertMessage(self, title: "Guest user", message: "As guest user you have limited access, please login with facebook for full access")
        }
    }
    
    func onChangeOrientation()
    {
        mapView!.frame = self.view.bounds
    }
    
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        //print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
    }
    
    func makeMark(coordinate:CLLocationCoordinate2D!, title:String!)
    {
        let  position = coordinate
        let marker = GMSMarker(position: position)
        
        marker.title = title
        
        switch(title)
        {
            case "\(InsidentType.Emergency)":
            marker.icon = UIImage(named: "emergengy_marker")
            case "\(InsidentType.Municipal)":
            marker.icon = UIImage(named: "municipal_marker")
            case "\(InsidentType.Security)":
            marker.icon = UIImage(named: "security_marker")
            case "\(InsidentType.Vial)":
            marker.icon = UIImage(named: "vial_marker")
        default:
            print("ningun icon")
            
        }
        marker.map = self.mapView
        allMarkers.append(marker)
    }
    
    
    
    func getIncidentsRequest()
    {
        //if(Utility.canExecuteAgain == false){return}
        
        Utility.showLoadingScreen(self.view)
        self.allMarkers.removeAll()
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://oculer.comlu.com/getIncidents.php")!)
        request.HTTPMethod = "POST"
        
        let postString = "locality=\(Utility.myLocality)"
        //print(postString)
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let response = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            //var error: NSError?
            let jsonData: NSData = response!.dataUsingEncoding(NSUTF8StringEncoding)!
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments)
                
                
                for(var i=0; i<json.count!; i++)
                {
//                    print(json[i].valueForKey("admin_area"))
//                    print(json[i].valueForKey("country"))
//                    print(json[i].valueForKey("date"))
//                    print(json[i].valueForKey("id"))
//                    print(json[i].valueForKey("latitude"))
//                    print(json[i].valueForKey("longitude"))
//                    print(json[i].valueForKey("place"))
//                    print(json[i].valueForKey("ref_place"))
//                    print(json[i].valueForKey("type"))
//                    print(json[i].valueForKey("user_id"))
//                    print(json[i].valueForKey("user_name"))
                    
                    let latitude:Double! = json[i].valueForKey("latitude")!.doubleValue
                    let longitude:Double! = json[i].valueForKey("longitude")!.doubleValue
                    let title:String! = json[i].valueForKey("type")! as! String
                    let id:Int! = json[i].valueForKey("id")!.integerValue!
                    let place:String! = json[i].valueForKey("place")! as! String
                    let ref:String! = json[i].valueForKey("ref_place")! as! String
                    let user_id:String! = json[i].valueForKey("user_id")! as! String
                    let dateStr:String! = json[i].valueForKey("date")! as! String
                    let country:String! = json[i].valueForKey("country")! as! String
                    let admin_area:String! = json[i].valueForKey("admin_area")! as! String
                    let locality:String! = json[i].valueForKey("locality")! as! String
                    
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    
                    let date:NSDate! = dateFormatter.dateFromString(dateStr)!
                    
                    dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
                    dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
                    let convertedDate = dateFormatter.stringFromDate(date)
                    
                    let coord:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    
                    let newIncident:Incident! = Incident(id: id, type: title, date: convertedDate, coord: coord, place: place, reference: ref, user_id: user_id,country: country,admin_area: admin_area,locality: locality)
                    
                    self.allIncidents.append(newIncident)
                    self.makeMark(coord, title: title)
                    
                }
                
                self.isUpdateMarkers = true
                //Utility.canExecuteAgain = true
                
            } catch {
                print("error serializing JSON: \(error)")
            }
            
        }
        task.resume()
    }
    
    func reverseGeocoding(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
    {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error)
                return
            }
            else if placemarks?.count > 0
            {
                let pm = placemarks![0]
                Utility.myLocality = pm.locality
                self.getIncidentsRequest()
            }
        })
    }


    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            self.mapView!.myLocationEnabled = true
            self.mapView!.settings.myLocationButton = true
            
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location = locations.first
        {
            self.mapView!.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
            
            Utility.localCoordinate = location.coordinate
            //print(locations.first?.coordinate)
            //locationManager.stopUpdatingLocation()
            
            if( Utility.myLocality == "" )
            {
                canMakeFirstRequest=true
            }
            
            
            
        }
    }
    
    func updateEndMarkersReuest()
    {
        if( canMakeFirstRequest == true && endFirstRequest == false)
        {
            reverseGeocoding(Utility.localCoordinate.latitude, longitude: Utility.localCoordinate.longitude)
            
            endFirstRequest = true
        }
        
        if(isUpdateMarkers == true)
        {
            Utility.hideLoadingScreen()
            isUpdateMarkers=false
            //print("Total markers: \(allMarkers.count)")
        }
        
    }
    
    func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        
        //print("Go to my location")
        
        return true
    }
    
  
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!)
    {
        //print(marker.title)
    }
    
//    func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
//        
//        
//        return nil
//    }
    
    
    //var currentPopUpView:UIView! = nil
    
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView!
    {
        let popUpView:PopUpView = PopUpView(frame: CGRect(x: 0, y: 0, width: 357, height: 178))
        
        for value in allIncidents
        {
            if(value.coord.latitude == marker.position.latitude && value.coord.longitude == marker.position.longitude)
            {
                Utility.incidentCountry = value.country
                Utility.incidentLocality = value.locality
                Utility.incidentPlace = value.place
                Utility.incidentThoroughfare = value.reference
                Utility.incidentAdministrativeArea = value.admin_area
                
                popUpView.place.text = "Place: \(Utility.getPlaceName())"
                popUpView.reference.text = "Reference: \(Utility.getReferenceeName())"
                
                break
            }
        }
        
        //currentPopUpView =
        
        return popUpView
    }
    

    
//    func updateMarkers()
//    {
//        getIncidentsRequest()
//    }

    
    
    
//    func getCurrentPlace()
//    {
//        placesClient?.currentPlaceWithCallback({
//            (placeLikelihoodList: GMSPlaceLikelihoodList?, error: NSError?) -> Void in
//            if let error = error {
//                print("Pick Place error: \(error.localizedDescription)")
//                return
//            }
//            
//            if let placeLicklihoodList = placeLikelihoodList {
//                let place = placeLicklihoodList.likelihoods.first?.place
//                if let place = place
//                {
//                    if place.formattedAddress != nil
//                    {
//                        print("getCurrentPlace function")
//                        
//                        print(place.name)
//                    }
//                    
//                }
//                
//            }
//            
//        })
//    }
    
//    func getCity(value: String) -> String
//    {
//        var inversed: [Character] = value.characters.reverse()
//        
//        var cont=0
//        for var caracter in inversed
//        {
//            if (caracter == "0" ||  caracter == "1" || caracter == "2" || caracter == "3" || caracter == "4" || caracter == "5" || caracter == "6" || caracter == "7" || caracter == "8" || caracter == "9" || caracter == "-")
//            {
//                cont++
//            }
//            
//        }
//        cont++
//        
//        
//        
//        for (var i=0;i<cont;i++)
//        {
//            inversed.removeLast()
//        }
//        
//        inversed = inversed.reverse()
//        
//        return String(inversed)
//    }
    

    @IBAction func updateMarkersAction(sender: AnyObject)
    {
        getIncidentsRequest()
    }
    
    @IBAction func addInsidentButton(sender: AnyObject)
    {
         self.performSegueWithIdentifier("newIncidentController", sender: self)
        //let popUpView:PopUpViewController = PopUpViewController()
        //self.presentViewController(popUpView, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
  
        NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: "updateEndMarkersReuest", userInfo: nil, repeats: true)
        
//         NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "updateMarkers", userInfo: nil, repeats: true)
        
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "onChangeOrientation", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        if(endFirstRequest == true)
        {
            getIncidentsRequest()
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
         NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        
        NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: "updateEndMarkersReuest", userInfo: nil, repeats: false)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
