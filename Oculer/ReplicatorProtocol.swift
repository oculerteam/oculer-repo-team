//
//  ReplicatorProtocol.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
//Methods that must be implemented by every class that extends it.
protocol ReplicatorProtocol {
    func fetchData()
    func processData(jsonResult: AnyObject?)
}