//
//  loginController.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/6/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AVFoundation

class loginController: UIViewController, FBSDKLoginButtonDelegate
{
    
    let loginButton: FBSDKLoginButton = FBSDKLoginButton()

    @IBOutlet weak var navigationTitle: UINavigationItem!

    @IBOutlet weak var goButtonRef: UIButton!
   
    @IBOutlet weak var profilePicture: FBSDKProfilePictureView!
    
    @IBOutlet weak var loginButtonContainer: UIView!
    
    @IBOutlet weak var typeLoggedUser: UIImageView!
    
    var player:AVPlayer?
    
    var videoBackgroundLayer:CALayer?
    
    let guestVideoBackground:String = "background_video2"
    let oculerVideoBackground:String = "background_video1"
    
    let guestGoButton1:String = "button5"
    let guestGoButton2:String = "button6"
    let oculerGoButton1:String = "button3"
    let oculerGoButton2:String = "button4"
    
    let oculerUser:String = "oculerUser"
    let guestUser:String = "guestUser"
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let image1:UIImage = UIImage(named: oculerGoButton1)!
        let image2:UIImage = UIImage(named: oculerGoButton2)!
        goButtonRef.setImage(image1, forState: UIControlState.Normal)
        goButtonRef.imageView!.animationImages = [image1, image2]
        goButtonRef.imageView!.animationDuration = 1.0
        goButtonRef.imageView!.startAnimating()
        
        let path = NSBundle.mainBundle().pathForResource(guestVideoBackground, ofType: "mp4")
        player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravityResize
        self.view.layer.insertSublayer(playerLayer, atIndex: 0)
        videoBackgroundLayer = playerLayer
        
        profilePicture.layer.cornerRadius = profilePicture.frame.width/2
        profilePicture.clipsToBounds = true
        
        loginButton.delegate = self
        
        
        loginButtonContainer.addSubview(loginButton)
        loginButton.frame = CGRectMake(0, 0, loginButtonContainer.frame.width, loginButtonContainer.frame.height)
        
        updateLoginFacebook()
        
    }
    
    
    func postConstruct()
    {
        profilePicture.layer.cornerRadius = profilePicture.frame.width/2
    }
    
    func onChangeOrientation()
    {
        
        profilePicture.layer.cornerRadius = profilePicture.frame.width/2
        
        if(videoBackgroundLayer != nil)
        {
            videoBackgroundLayer!.frame = self.view.frame
            
        }
        
    }
    
    func toogleGuestAndRegister(value: TypeUser)
    {
        let videoName:String = value == TypeUser.Oculer ? guestVideoBackground : oculerVideoBackground
        
        let path = NSBundle.mainBundle().pathForResource(videoName, ofType: "mp4")
        player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravityResize
        self.view.layer.replaceSublayer(videoBackgroundLayer!, with: playerLayer)
        videoBackgroundLayer = playerLayer
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerItemDidReachEnd", name: AVPlayerItemDidPlayToEndTimeNotification, object: player!.currentItem)
        
        playerItemDidReachEnd()
        
        let imageName1:String = value == TypeUser.Oculer ? oculerGoButton1 : guestGoButton1
        let imageName2:String = value == TypeUser.Oculer ? oculerGoButton2 : guestGoButton2
        
        let image1:UIImage = UIImage(named: imageName1 )!
        let image2:UIImage = UIImage(named: imageName2)!
        goButtonRef.setImage(image1, forState: UIControlState.Normal)
        goButtonRef.imageView!.animationImages = [image1, image2]
        goButtonRef.imageView!.animationDuration = 1.0
        goButtonRef.imageView!.startAnimating()
        
        
        let typeUser:String = value == TypeUser.Oculer ? oculerUser : guestUser
        typeLoggedUser.image = UIImage(named: typeUser)
                
        Utility.typeUser=value
        
    }
    
    
    func updateLoginFacebook()
    {
        if FBSDKAccessToken.currentAccessToken() != nil
        {
            Utility.showLoadingScreen(self.view)
            //parameters
            //["fields":"id,email,name,picture.width(480).height(480)"]
            FBSDKGraphRequest(graphPath: "me", parameters: nil).startWithCompletionHandler { (graphConnectionRequest, result, error) -> Void in
                
                Utility.userFacebookID = (result.valueForKey("id")! as! String)
                Utility.userFacebookName = (result.valueForKey("name")! as! String)
                
                self.navigationTitle.title = "Oculer"
                self.toogleGuestAndRegister(TypeUser.Oculer)
                Utility.hideLoadingScreen()
                print(Utility.userFacebookID!)
                print(Utility.userFacebookName!)
                //self.testRef.image = self.getProfilePicture(FB_ID)                
                
            }
            
        }
        else
        {
            //self.navigationTitle.title = "You are Logged Out"
            self.navigationTitle.title = "Guess"
            self.toogleGuestAndRegister(TypeUser.Guest)
        }
        
    }
    
 
    func getProfilePicture(fid: String) -> UIImage? {
        if (fid != "") {
            let imgURLString = "https://graph.facebook.com/" + fid + "/picture?type=small" //type=small, normal, album, large, square"
            let imgURL = NSURL(string: imgURLString)
            let imageData = NSData(contentsOfURL: imgURL!)
            let image = UIImage(data: imageData!)
            return image
        }
                
        return nil
    }
    
   
    
    @IBAction func goButton(sender: AnyObject)
    {
        self.performSegueWithIdentifier("mainViewController", sender: self)
    }
    
    
    func playerItemDidReachEnd() {
        player!.seekToTime(kCMTimeZero)
        player!.play()
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        
        graphRequest.startWithCompletionHandler { (graphConnectionRequest, result, error) -> Void in
            
        }
        
        updateLoginFacebook()
        
    }
    
    
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
        self.navigationTitle.title = "Guess"
        self.toogleGuestAndRegister(TypeUser.Guest)
        
    }    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
        
        player!.pause()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onChangeOrientation", name: UIDeviceOrientationDidChangeNotification, object: nil)
        NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: "postConstruct", userInfo: nil, repeats: false)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerItemDidReachEnd", name: AVPlayerItemDidPlayToEndTimeNotification, object: player!.currentItem)
        playerItemDidReachEnd()
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
