//
//  ProfileAPI.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
import CoreData

/**
 Profile API contains the endpoints to Create/Read/Update/Delete Profiles.
 */
class ProfileAPI {
    
    private let persistenceManager: PersistenceManager!
    private var mainContextInstance:NSManagedObjectContext!

 
    
    private let shareFacebook = ProfileAttributes.shareFacebook.rawValue
    private let showMunicipal = ProfileAttributes.showMunicipal.rawValue
    private let showEmergency = ProfileAttributes.showEmergency.rawValue
    private let showVial = ProfileAttributes.showVial.rawValue
    private let showSecurity = ProfileAttributes.showSecurity.rawValue
    private let distancePost = ProfileAttributes.distancePost.rawValue
    
    //Utilize Singleton pattern by instanciating ProfileAPI only once.
    class var sharedInstance: ProfileAPI {
        struct Singleton {
            static let instance = ProfileAPI()
        }
        
        return Singleton.instance
    }
    
    init() {
        self.persistenceManager = PersistenceManager.sharedInstance
        self.mainContextInstance = persistenceManager.getMainContextInstance()
    }
    
    
    func saveProfile(profileDetails: Dictionary<String, AnyObject>) {
        
        //Minion Context worker with Private Concurrency type.
        let minionManagedObjectContextWorker:NSManagedObjectContext =
        NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        
        //Create new Object of Profile entity
        let profileItem = NSEntityDescription.insertNewObjectForEntityForName("Profile",
            inManagedObjectContext: minionManagedObjectContextWorker) as! Profile
        
        //Assign field values
        for (key, value) in profileDetails {
            for attribute in ProfileAttributes.getAll {
                if (key == attribute.rawValue) {
                    profileItem.setValue(value, forKey: key)
                }
            }
        }
        
        //Save current work on Minion workers
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        
        //Save and merge changes from Minion workers with Main context
        self.persistenceManager.mergeWithMainContext()
        
        //Post notification to update datasource of a given Viewcontroller/UITableView
        self.postUpdateNotification()
    }
    
    
    
    // MARK: Read
    
    /**
    Retrieves all profiles items stored in the persistence layer, default (overridable)
    parameters:
    
    - Parameter sortedByDate: Bool flag to add sort rule: by Date
    - Parameter sortAscending: Bool flag to set rule on sorting: Ascending / Descending date.
    
    - Returns: Array<Profile> with found Profiles in datastore
    */
    func getAllProfiles(sortedByDate:Bool = true, sortAscending:Bool = true) -> Array<Profile> {
        var fetchedResults:Array<Profile> = Array<Profile>()
        
        // Create request on Profile entity
        let fetchRequest = NSFetchRequest(entityName: "Profile")
        
        
        //Execute Fetch request
        do {
            fetchedResults = try  self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Profile]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Profile>()
        }
        
        return fetchedResults
    }
    
    
    
    func updateProfile(profileItemToUpdate: Profile, newProfileItemDetails: Dictionary<String, AnyObject>){
        
        let minionManagedObjectContextWorker:NSManagedObjectContext =
        NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        
        //Assign field values
        for (key, value) in newProfileItemDetails {
            for attribute in ProfileAttributes.getAll {
                if (key == attribute.rawValue) {
                    profileItemToUpdate.setValue(value, forKey: key)
                }
            }
        }
        
        //Persist new Profile to datastore (via Managed Object Context Layer).
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()
        
        self.postUpdateNotification()
    }
    
    
    /**
     Delete a single Profile item from persistence layer.
     
     - Parameter profileItem: Profile to be deleted
     - Returns: Void
     */
    func deleteProfile(ProfileProfile: Profile) {
        //Delete Profile item from persistance layer
        self.mainContextInstance.deleteObject(ProfileProfile)
        self.postUpdateNotification()
    }
    
    /**
     Post update notification to let the registered listeners refresh it's datasource.
     
     - Returns: Void
     */
    private func postUpdateNotification(){
        NSNotificationCenter.defaultCenter().postNotificationName("updateProfileTableData", object: nil)
    }
    
    
    /**
     Create new Profiles from a given list, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
    */
    func saveProfileList(profileList:Array<AnyObject>){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
            
            //Minion Context worker with Private Concurrency type.
            let minionManagedObjectContextWorker:NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            minionManagedObjectContextWorker.parentContext = self.mainContextInstance
            
            //Create ProfileEntity, process member field values
            for index in 0..<profileList.count {
                var profileItem:Dictionary<String, NSObject> = profileList[index] as! Dictionary<String, NSObject>
                
                //Check that an Profile to be stored has a date, title and city.
                if profileItem[self.distancePost] != "" {
                    
                    //Create new Object of Profile entity
                    let item = NSEntityDescription.insertNewObjectForEntityForName("Profile",
                        inManagedObjectContext: minionManagedObjectContextWorker) as! Profile
                    
                    //Add member field values
                    item.setValue(profileItem[self.distancePost], forKey: self.distancePost)
                    item.setValue(profileItem[self.shareFacebook], forKey: self.shareFacebook)
                    item.setValue(profileItem[self.showEmergency], forKey: self.showEmergency)
                    item.setValue(profileItem[self.showMunicipal], forKey: self.showMunicipal)
                    item.setValue(profileItem[self.showVial], forKey: self.showVial)
                    item.setValue(profileItem[self.showSecurity], forKey: self.showVial)
                    
                    //Save current work on Minion workers
                    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
                }
            }
            
            //Save and merge changes from Minion workers with Main context
            self.persistenceManager.mergeWithMainContext()
            
            //Post notification to update datasource of a given Viewcontroller/UITableView
            dispatch_async(dispatch_get_main_queue()) {
                self.postUpdateNotification()
            }
        })
    }
    
}