//
//  ProfileTableViewCell.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var share: UISwitch!
    @IBOutlet weak var distance: UISlider!
    @IBOutlet weak var vial: UISwitch!
    @IBOutlet weak var security: UISwitch!
    @IBOutlet weak var emergency: UISwitch!
    @IBOutlet weak var municipal: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
