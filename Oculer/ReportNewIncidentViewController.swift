//
//  ReportNewIncidentViewController.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/24/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import UIKit
import GoogleMaps
import AddressBookUI

class ReportNewIncidentViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate
{
    
    @IBOutlet weak var vialButtonRef: UIButton!
    @IBOutlet weak var securityButtonRef: UIButton!
    @IBOutlet weak var emergencyButtonRef: UIButton!
    @IBOutlet weak var municipalButtonRef: UIButton!
    
    @IBOutlet weak var insidentNameRef: UILabel!
    
    @IBOutlet weak var placeNameRef: UILabel!
    @IBOutlet weak var streetNameRef: UILabel!
    @IBOutlet weak var incidentTimeRef: UIDatePicker!
    @IBOutlet weak var currentDateRef: UILabel!
    @IBOutlet weak var pictureRef: UIImageView!
    @IBOutlet weak var commentRef: UITextField!
    
    
    let vialImage1:String="trafico1"
    let vialImage2:String="trafico2"
    let securityImage1:String="seguridad1"
    let securityImage2:String="seguridad2"
    let emergencyImage1:String="emergencia1"
    let emergencyImage2:String="emergencia2"
    let municipalImage1:String="municipal1"
    let municipalImage2:String="municipal2"
    
    
    var imagePicker: UIImagePickerController!
    var animateDistance = CGFloat()
    var kbHeight: CGFloat!
    
    var locationManager:CLLocationManager?
    var placePicker: GMSPlacePicker?
    
    var placeCountry:String?
    var placeCity:String?
    
    var isEndRequest:Bool! = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy";        
        let strDate = dateFormatter.stringFromDate(incidentTimeRef.date)
        
        currentDateRef.text = strDate
        
        commentRef.delegate=self
        //clearAllFilesFromTempDirectory()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func loadChooseLocationScreen()
    {
        let center = Utility.localCoordinate!
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        
        placePicker = GMSPlacePicker(config: config)
        
        placePicker?.pickPlaceWithCallback({ (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                
                let meLocation : CLLocation = CLLocation(latitude: Utility.localCoordinate.latitude, longitude: Utility.localCoordinate.longitude)
                
                let incidentLocation : CLLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                
                let meters = incidentLocation.distanceFromLocation(meLocation)
                
                if(meters > 10000)
                {
                    Utility.alertMessage(self, title: "Warning", message: "You cant report an incidet greater than 10km of distance")
                    //return
                }

                
                if place.formattedAddress != nil
                {
                    self.reverseGeocoding(place.coordinate.latitude, longitude: place.coordinate.longitude, placeNmae: place.name)
                }
                else
                {
                    self.reverseGeocoding(place.coordinate.latitude, longitude: place.coordinate.longitude)
                }
                
            }
            else
            {
                self.placeNameRef.text = ""
                self.streetNameRef.text = ""
                
            }
            //self.dismissViewControllerAnimated(true, completion: nil)
        })
        
    }
    
    
    
    func reverseGeocoding(latitude: CLLocationDegrees, longitude: CLLocationDegrees, placeNmae: String? = nil) {
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error)
                return
            }
            else if placemarks?.count > 0
            {
                let pm = placemarks![0]
                Utility.incidentThoroughfare = pm.thoroughfare 
                Utility.incidentLocality = pm.locality
                Utility.incidentAdministrativeArea = pm.administrativeArea
                Utility.incidentCountry = pm.country
                Utility.incidentPlace = placeNmae
                Utility.incidentCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                
                self.placeNameRef.text = Utility.getPlaceName()
                self.streetNameRef.text = Utility.getReferenceeName()
                
            }
        })
    }
    
        
    func keyboardWillShow(notification: NSNotification)
    {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                kbHeight = keyboardSize.height
                self.animateTextField(true)
            }
        }
        //print("keyboard show")
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.animateTextField(false)
        //print("keyboard hide")
        
    }
    
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(path, atomically: true)
        
        return result
        
    }
    
    func animateTextField(up: Bool) {
        let movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
        })
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        return true
    }
    
   
    
    func onChangeOrientation()
    {
        //self.view.frame = CGRectOffset(self.view.frame, 0, kbHeight)
        //view.endEditing(true)
    }
    
    
    func setInsident(value:InsidentType)
    {
        Utility.insidentType = value
        insidentNameRef.text = "\(value)"
        
        var simpleImage:UIImage = UIImage(named: vialImage1 )!
        vialButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
        
        simpleImage = UIImage(named: securityImage1 )!
        securityButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
        
        simpleImage = UIImage(named: emergencyImage1 )!
        emergencyButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
        
        simpleImage = UIImage(named: municipalImage1 )!
        municipalButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
        
        
        
        switch(value)
        {
            case InsidentType.Vial:
                simpleImage = UIImage(named: vialImage2 )!
                vialButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
            
            case InsidentType.Emergency:
                simpleImage = UIImage(named: emergencyImage2 )!
                emergencyButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
            
            case InsidentType.Security:
                simpleImage = UIImage(named: securityImage2 )!
                securityButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
            
            case InsidentType.Municipal:
                simpleImage = UIImage(named: municipalImage2 )!
                municipalButtonRef.setImage(simpleImage, forState: UIControlState.Normal)
            
        default:
            
            print("Error switch in: setInsident")
            
        }
    
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        
        return fileURL.path!
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        pictureRef.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if( pictureRef.image != nil)
        {
            if(pictureRef.image!.size.width >= pictureRef.image!.size.height )
            {
                pictureRef.image = resizeImageByWidth(pictureRef.image!, newWidth: 480)
            }
            else
            {
                pictureRef.image = resizeImageByHeight(pictureRef.image!, newHeight: 480)
            }
            
        }
        
        //print("width: \(pictureRef.image!.size.width), height: \(pictureRef.image!.size.height)")
        
        
    }
    
//    func loadImageFromPath(path: String) -> UIImage? {
//        
//        let image = UIImage(contentsOfFile: path)
//        
//        if image == nil {
//            
//            print("missing image at: \(path)")
//        }
//        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
//        return image
//        
//    }
    func clearAllFilesFromTempDirectory()
    {
        let fileURL = getDocumentsURL().URLByAppendingPathComponent("")
        let directoryPath = fileURL.path!
        var fullPath = directoryPath
        
        do{
            
            let directoryContents: NSArray = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(directoryPath)
            
            if directoryContents.count > 0
            {
                for path in directoryContents
                {
                    do
                    {
                        fullPath = directoryPath + (path as! String)
                        try NSFileManager.defaultManager().removeItemAtPath(fullPath)
                        print("deleted:  \(fullPath)")
                    }
                    catch
                    {
                        print("cant delete: \(fullPath)")
                    }
                    
                    
                }
            }
            else
            {
                print("directorio vacio: \(fullPath)")
            }
            
            
        }
        catch
        {
            print("error: \(directoryPath)")
            
        }
        
    }
    
    func resizeImageByWidth(sourceImage:UIImage!, newWidth:CGFloat) -> UIImage
    {
        let scaleFactor = newWidth / sourceImage.size.width
        let newHeight = sourceImage.size.height * scaleFactor
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        sourceImage.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    
    func resizeImageByHeight(sourceImage:UIImage!, newHeight:CGFloat) -> UIImage
    {
        
        let scaleFactor = newHeight / sourceImage.size.height
        let newWidth = sourceImage.size.width * scaleFactor
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        sourceImage.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    
//    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage! {
//        let context = CIContext(options: nil)
//        
//        return context.createCGImage(inputImage, fromRect: inputImage.extent)
//    }
//    
//    func resizeImage(image:UIImage!) -> UIImage
//    {
//        let ciImage = CIImage(image: image)
//        
//        let image = CIImage(CGImage: convertCIImageToCGImage(ciImage!))
//        
//        let filter = CIFilter(name: "CILanczosScaleTransform")!
//        filter.setValue(image, forKey: "inputImage")
//        filter.setValue(0.5, forKey: "inputScale")
//        filter.setValue(1.0, forKey: "inputAspectRatio")
//        let outputImage = filter.valueForKey("outputImage") as! CIImage
//        
//        let context = CIContext(options: [kCIContextUseSoftwareRenderer: false])
//        return UIImage(CGImage: context.createCGImage(outputImage, fromRect: outputImage.extent))
//    }
    
    func makeIncidentRequest()
    {
        Utility.showLoadingScreen(self.view)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://oculer.comlu.com/Insert_incident.php")!)
        request.HTTPMethod = "POST"
        
        let postString = "type=\(Utility.insidentType)&date=\(incidentTimeRef.date)&latitude=\(Utility.incidentCoordinate!.latitude)&longitude=\(Utility.incidentCoordinate!.longitude)&country=\(Utility.incidentCountry)&admin_area=\(Utility.incidentAdministrativeArea)&locality=\(Utility.incidentLocality)&ref_place=\(Utility.incidentThoroughfare)&place=\(Utility.incidentPlace)&user_id=\(Utility.userFacebookID)&user_name=\(Utility.userFacebookName)&active=1"
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let response:String! = NSString(data: data!, encoding: NSUTF8StringEncoding) as! String
            let incident_ID:Int! = Int(response!)
            
            
            if self.commentRef.text! != ""
            {
                //self.isEndRequest = false
                self.makeCommentRequest(incident_ID)
            }
            if self.pictureRef.image != nil
            {
                //self.isEndRequest = false
                self.makePictureRequest(incident_ID)
            }
            
            self.isEndRequest = true
            
        }
        task.resume()        
    }
    
    func makeCommentRequest(incidentID:Int!)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://oculer.comlu.com/Insert_comment.php")!)
        request.HTTPMethod = "POST"
        
        let postString = "id_incident=\(incidentID)&comment=\(commentRef.text!)&user_id=\(Utility.userFacebookID)&user_name=\(Utility.userFacebookName)&active=1"
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response!)")
            }
        }
        task.resume()
    }
    
    func makePictureRequest(incidentID:Int!)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://oculer.comlu.com/Insert_image.php")!)
        request.HTTPMethod = "POST"
        
        let path:String! = "IncidentPictures/\(Utility.userFacebookID)_\(incidentTimeRef.date).jpg"
        
        let imageData:NSData! = UIImageJPEGRepresentation(pictureRef.image!, 0.5)
        
        let base64String:String! = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        
        let postString = "id_incident=\(incidentID)&path=\(path)&user_id=\(Utility.userFacebookID)&user_name=\(Utility.userFacebookName)&active=1&image=\(base64String)"
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response!)")
            }
            
        }
        task.resume()
    }
    
    
    
    func update()
    {
        
        if( isEndRequest == true )
        {
            Utility.hideLoadingScreen()
            isEndRequest = false
            Utility.alertMessage(self, title: "Success", message: "Your Incidents was successful reported", completion: { (UIAlertAction) -> Void in
                
                self.dismissViewControllerAnimated(true, completion: nil)
                
            })
            
        }
    
    }
    
    
    @IBAction func uploadImageFromCameraAction(sender: AnyObject)
    {
        
        imagePicker =  UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func uploadImageFromGaleryAction(sender: UIButton)
    {
        imagePicker =  UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }

    @IBAction func cancelAction(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func makeNewIncidentAction(sender: UIButton)
    {
        if(Utility.insidentType == InsidentType.None)
        {
            Utility.alertMessage(self, title: "Warning", message: "You need to specify the kind of Incident")
            return
        }
        
        if(Utility.incidentCoordinate == nil)
        {
            Utility.alertMessage(self, title: "Warning", message: "You need to specify coordinate location")
            return
        }
        
      
        
        makeIncidentRequest()
        
        //print(incidentTimeRef.date)
        
//        if(pictureRef.image == nil)
//        {
//            return
//        }
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMM dd, yyyy";
//        let strDate = dateFormatter.stringFromDate(incidentTimeRef.date)
//        
//        let imagePath = fileInDocumentsDirectory("Insident_\(Utility.insidentType)_" + incidentTimeRef.date.description + ".png")
//        saveImage(self.pictureRef.image!, path: imagePath)
//        print("Image save at: " + imagePath)
        
    }
   
    
   
    @IBAction func insidentTimeAction(sender: AnyObject)
    {
        let dateFormatter = NSDateFormatter()
        
        //dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        //dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        //dateFormatter.dateFormat = "HH:mm:ss";
        dateFormatter.dateFormat = "MMMM dd, yyyy";
        
        let strDate = dateFormatter.stringFromDate(incidentTimeRef.date)
        
        
        print(strDate)
        print(incidentTimeRef.date)
        
    }
    
    @IBAction func setLocationAction(sender: UIButton)
    {
        
        loadChooseLocationScreen()
        //self.performSegueWithIdentifier("selectNewMapLocation", sender: self)
        
    }
    
    @IBAction func vialButtonAction(sender: AnyObject)
    {
        setInsident(InsidentType.Vial)
    }
    
    @IBAction func securityButtonAction(sender: AnyObject)
    {
        setInsident(InsidentType.Security)
    }
    
    @IBAction func emergencyButtonAction(sender: AnyObject)
    {
        setInsident(InsidentType.Emergency)
    }
    
    @IBAction func municipalButtonAction(sender: AnyObject)
    {
        setInsident(InsidentType.Municipal)
    }
  
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
        
         NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: "update", userInfo: nil, repeats: false)
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        
        NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: "update", userInfo: nil, repeats: true)
        // Keyboard stuff.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onChangeOrientation", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //if segue.identifier == "selectNewMapLocation" {
            
            //let secondViewController = segue.destinationViewController as! SelectNewMapLocationController
            //secondViewController.firstViewController = self
        //}
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        placePicker?.delete(self)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

