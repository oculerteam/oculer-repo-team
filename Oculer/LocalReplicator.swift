//
//  LocalReplicator.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
/**
 Local Replicator handles reading and parsing JSON data from a local file and calls the Core Data Stack,
 (via profileAPI) to actually create Core Data Entities and persist to SQLite Datastore.
 */
class LocalReplicator : ReplicatorProtocol {
    
    private var profileAPI: ProfileAPI!
    
    //Utilize Singleton pattern by instanciating Replicator only once.
    class var sharedInstance: LocalReplicator {
        struct Singleton {
            static let instance = LocalReplicator()
        }
        
        return Singleton.instance
    }
    
    init() {
        self.profileAPI = ProfileAPI.sharedInstance
    }
    
    /**
     Pull Profile data from a given resource, posts a notification to update
     datasource of a given/listening ViewController/UITableView
     
     - Returns: Void
     */
    func fetchData() {
        //Read JSON file in seperate thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            // read JSON file, parse JSON data
            self.processData(self.readFile())
            
            // Post notification to update datasource of a given ViewController/UITableView
            dispatch_async(dispatch_get_main_queue()) {
                NSNotificationCenter.defaultCenter().postNotificationName("updateProfileTableData", object: nil)
            }
        })
    }
    
    /**
     Read JSON data from a local file in the Resources dir.
     
     - Returns: AnyObject The contents of the JSON file.
     */
    func readFile() -> AnyObject {
        let dataSourceFilename:String = "profiles"
        let dataSourceFilenameExtension:String = "json"
        let filemgr = NSFileManager.defaultManager()
        let currPath = NSBundle.mainBundle().pathForResource(dataSourceFilename, ofType: dataSourceFilenameExtension)
        var jsonResult:AnyObject! = nil
        
        do {
            let jsonData = NSData(contentsOfFile: currPath!)!
            
            if filemgr.fileExistsAtPath(currPath!) {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)
            } else {
                print("\(dataSourceFilename).\(dataSourceFilenameExtension)) does not exist, therefore cannot read JSON data.")
            }
        } catch let fetchError as NSError {
            print("read file error: \(fetchError.localizedDescription)")
        }
        
        return jsonResult
    }
    /**
     Process data from a given resource Event objects and assigning
     (additional) property values and calling the Event API to persist Events
     to the datastore.
     
     - Parameter jsonResult: The JSON content to be parsed and stored to Datastore.
     - Returns: Void
     */
    func processData(jsonResult:AnyObject?) {
        var retrievedProfiles = [Dictionary<String,AnyObject>]()
        
        
        
        if let profileList = jsonResult  {
            for index in 0..<profileList.count {
                var range = [Int](index..<profileList.count)
                var profileItem:Dictionary<String, AnyObject> = profileList[range] as! Dictionary<String, AnyObject>
                retrievedProfiles.append(profileItem)
            }
        }
        
        //Call Event API to persist Event list to Datastore
        profileAPI.saveProfileList(retrievedProfiles)
    }
}