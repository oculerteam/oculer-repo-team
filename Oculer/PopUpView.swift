//
//  PopUpView.swift
//  Oculer
//
//  Created by Christian Bustamante on 12/23/15.
//  Copyright © 2015 Christian Bustamante. All rights reserved.
//

import UIKit

@IBDesignable class PopUpView: UIView
{
    
    @IBOutlet weak var place: UITextView!
    @IBOutlet weak var reference: UITextView!
    
    
    var thisView:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        thisView = loadViewFromNib()
        thisView.frame = bounds
        thisView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        
        addSubview(thisView)
        
    }
    
    func loadViewFromNib() ->UIView
    {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "PopUpView", bundle: bundle)
        let view:UIView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
        
    }
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
