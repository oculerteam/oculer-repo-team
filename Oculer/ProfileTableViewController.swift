//
//  ProfileTableViewController.swift
//  Oculer
//
//  Created by Andres Munoz on 5/1/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    private var profileList:Array<Profile> = []
    private var filteredProfileList:Array<Profile> = []
    private var selectedProfileItem : Profile!
    private var profileAPI: ProfileAPI!
    private let profileTableCellIdentifier = "profileItemCell"
    private let showProfileItemSegueIdentifier = "showProfileItemSegue"
    private let editProfileItemSegueIdentifier = "editProfileItemSegue"
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        //Register for notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateProfileTableData:", name: "updateProfileTableData", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setStateLoading:", name: "setStateLoading", object: nil)
        
        self.profileAPI = ProfileAPI.sharedInstance
        self.profileList =  self.profileAPI.getAllProfiles()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return profileList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let profileCell =
        
        tableView.dequeueReusableCellWithIdentifier(profileTableCellIdentifier, forIndexPath: indexPath) as! ProfileTableViewCell
        
        let profileItem:Profile!
        
        profileItem = profileList[indexPath.row]
       
        profileCell.share.selected = profileItem.shareFacebook
        profileCell.municipal.selected =  profileItem.showMunicipal
        profileCell.security.selected =  profileItem.showSecurity
        profileCell.emergency.selected = profileItem.showEmergency
        profileCell.vial.selected = profileItem.showVial
        profileCell.distance.value = 10
        
        return profileCell
    }
    
    // MARK: - Table edit mode
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //Delete item from datastore
            profileAPI.deleteProfile(profileList[indexPath.row])
            //Delete item from tableview datascource
            profileList.removeAtIndex(indexPath.row)
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            self.title = "Profile"
        }
    }
    
    // MARK: - Search
    
    /**
    Calls the filter function to filter results by searchbar input
    
    - Parameter searchController: passed Controller to get text from
    - Returns: Void
    */
    func updateSearchResultsForSearchController(searchController: UISearchController) {
       
        refreshTableData()
    }
    
    
    
    func updateProfileTableData(notification: NSNotification) {
        refreshTableData()
        self.activityIndicator.hidden = true
        self.activityIndicator.stopAnimating()
    }
    
    func setStateLoading(notification: NSNotification) {
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()
    }
    
    /**
     Refresh table data
     
     - Returns: Void
     */
    private func refreshTableData(){
        self.profileList.removeAll(keepCapacity: false)
        self.profileList = self.profileAPI.getAllProfiles()
        self.tableView.reloadData()
        self.title = "Profile"
    }
    
    /**
     Retrieve image from remote or cache.
     
     - Returns: Void
     */
    private func getProfileImage(indexPath: NSIndexPath) -> UIImage {
        //TODO
        
        //Check if local image is cached, if not use GCD to download and display it.
        //Use indexPath as reference to cell to be updated.
        
        //For now load from image assets locally.
        return UIImage(named: "ProfileImageSecond")!
    }
}

