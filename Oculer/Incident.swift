//
//  Incident.swift
//  Oculer
//
//  Created by Christian Bustamante on 1/4/16.
//  Copyright © 2016 Christian Bustamante. All rights reserved.
//

import Foundation
import GoogleMaps

class Incident
{
    var id:Int!
    var type:String!
    var date:String!
    var coord:CLLocationCoordinate2D!
    var place:String!
    var reference:String!
    var country:String!
    var admin_area:String!
    var locality:String!
    var user_id:String!
    
    
    init(id:Int!, type:String!, date:String!, coord:CLLocationCoordinate2D!, place:String!, reference:String!, user_id:String!, country:String!,admin_area:String!,locality:String!)
    {
        self.id=id
        self.type = type
        self.date = date
        self.coord = coord
        self.place = place
        self.reference = reference
        self.user_id = user_id
        self.country=country
        self.admin_area = admin_area
        self.locality = locality
    }


}
